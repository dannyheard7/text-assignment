import matplotlib.pyplot as plt
import numpy as np

X = [0,1,2,3]

my_xticks = ["None", "Stemming", "Stoplist", "Both"]
plt.xticks(X, my_xticks)

binary = np.array([0.09,0.10,0.12,0.13])
tf = np.array([0.07, 0.10, 0.15,0.17])
tfidf= np.array([0.18,0.23,0.19,0.24])
plt.yticks(np.arange(tf.min(), 0.28, 0.02))

plt.plot(X, binary)
plt.plot(X, tf)
plt.plot(X, tfidf)

plt.legend(['Binary', 'TF', 'TFIDF'], loc='upper left')

plt.xlabel('Configuration')
plt.ylabel('F1 Score')

plt.title("IR Performance")


plt.show()