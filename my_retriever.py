import math

class Retrieve:
    # Create new Retrieve object storing index and termWeighting scheme
    def __init__(self, index, termWeighting):
        self.index = index
        self.termWeighting = termWeighting

    # Method to apply query to index
    def forQuery(self, query):
        if self.termWeighting == "tf":
            doc_ids = self.term_frequency_weighting(query)
        elif self.termWeighting == "tfidf":
            doc_ids = self.tfidf_weigting(query)
        else:
            doc_ids = self.binary_weighting(query)

        return doc_ids

    # Remove terms that are not in the query
    def get_candidate_index(self, query):
        candidate_index = {term: self.index[term] for term in query if term in self.index}

        return candidate_index

    def binary_weighting(self, query):
        doc_ids = {}
        doc_squared_vectors = {}

        # For binary weighting, only terms that are in the query are relevant
        candidate_index = self.get_candidate_index(query) 

        for term, docs in candidate_index.items():
            for doc_id, count in docs.items():
                if doc_id in doc_ids:
                    doc_ids[doc_id][term] = 1 # Binary weighting: 1 if term is present in doc & search, 0 otherwise
                    doc_squared_vectors[doc_id] += 1 # Squared vector length, 1^2 = 1
                else:
                    doc_ids[doc_id] = {term: 1}
                    doc_squared_vectors[doc_id] = 1

        query = {term: 1 for term in query.keys()} # 1 or 0 in binary search

        return self.vector_space_model(doc_ids, query, doc_squared_vectors)
        
    def term_frequency_weighting(self, query):
        doc_ids = {}
        doc_squared_vectors = {}
        
        # For tf weighting, all terms are relevant as we need to calculate the length of the document vector in order to rank results accurately
        for term, docs in self.index.items():
            for doc_id, count in docs.items():
                if doc_id in doc_ids:
                    doc_ids[doc_id][term] = count # Weighting is term frequency in document
                    doc_squared_vectors[doc_id] += count ** 2 # Squared vector length
                else:
                    doc_ids[doc_id] = {term: count}
                    doc_squared_vectors[doc_id] = count ** 2

        # Query weighted terms are already calculated (term => count)

        return self.vector_space_model(doc_ids, query, doc_squared_vectors)

    def tfidf_weigting(self, query): 
        # Calculate collection size by getting a set of all the document ids, then take the length of that set
        collection_size = len({doc_id for doc_ids in self.index.values() for doc_id in doc_ids})

        doc_ids = {}
        doc_squared_vectors = {}

        for term, docs in self.index.items():
            document_frequency = len(docs)
            idf = math.log(collection_size / document_frequency) # Idf = collection size / document frequency (num docs with term)

            for doc_id, tf in docs.items():             
                tfidf = idf * tf

                if doc_id in doc_ids:
                    doc_ids[doc_id][term] = tfidf # Weighting is term frequency in document * idf
                    doc_squared_vectors[doc_id] += tfidf ** 2 # Squared vector length
                else:
                    doc_ids[doc_id] = {term: tfidf}
                    doc_squared_vectors[doc_id] = tfidf ** 2            

        weighted_query = {}

        # Also need to calculate weighted values for terms in query
        # No need to calculate squared vector length as it is the same for all documents
        for term, tf in query.items():
            if term in self.index:
                document_frequency = len(self.index[term])

                idf = math.log(collection_size / document_frequency)
                tfidf = idf * tf

                weighted_query[term] = tfidf

        return self.vector_space_model(doc_ids, weighted_query, doc_squared_vectors)

    def vector_space_model(self, doc_ids, query, weighted_squared_doc_vectors):
        cosine_values = {}

        for doc_id, doc_terms in doc_ids.items():
            dot_product = 0
            
            for query_term, weighted_query_term in query.items():
                if query_term in doc_terms.keys(): 
                    weighted_doc_term = doc_terms[query_term]
                    dot_product += (weighted_doc_term * weighted_query_term)

            if dot_product != 0:  
                doc_vector_length = math.sqrt(weighted_squared_doc_vectors[doc_id])
                cosine_value = dot_product / doc_vector_length
                # No need to calculate squared vector length for the query as it is the same for all documents
                
                cosine_values[doc_id] = cosine_value

        # Sort doc_ids by cosine value in descending order
        sorted_doc_ids = sorted(cosine_values, key=cosine_values.get, reverse=True)
        return sorted_doc_ids
